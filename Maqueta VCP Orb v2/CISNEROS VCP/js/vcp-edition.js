
$(document).ready(function () {

    kpi_input = _.template($('#kpi-input').html());
    kpi_fill = _.template($('#kpi-fill').html());
    kpi_toggle = _.template($('#kpi-toggle').html());

    if ($('#vcp-wizard').length) {
        $('#vcp-wizard').steps({
            bodyTag: 'fieldset',
            autoFocus: true,
            transitionEffect: 'slideLeft',
            finish: 'Continue',
            onStepChanging: function (e, i, j) {
                console.log($('#vcp-wizard fieldset.current').attr('id')); //warn before continue
                // return false; //prevents from steping
                $("#steps-wizard").validate().settings.ignore = ":disabled,:hidden";
                return $("#steps-wizard").valid();
            },
            onStepChanged: function () {
                resizeWizardHeight();
                locateGlobalActual();
            },
            onFinishing: function () {
                $("#steps-wizard").validate().settings.ignore = ":disabled";
                return $("#steps-wizard").valid();
            },
            onFinished: function () {
                $("#steps-wizard").submit();
            }
        });
    }

    if ($('#steps-wizard').length) {
        $('#steps-wizard').validate({
            // Rules for form validation
            rules: {
                name: {
                    required: true
                },
                digits: {  
                    digits: true
                },
                total: {
                    max: 100,
                    min: 100
                },
                year: {
                    required: true,
                    digits: true
                }
            },

            // Messages for form validation
            messages: {
                name: {
                    required: 'Please enter your first name'
                },
                digits: {
                    digits: 'Digits only please'
                },
                total: {
                    min: 'Distribution must be 100',
                    max: 'Distribution must be 100'
                },
                year: {
                    required: 'Enter year',
                    digits: 'Digits only please'
                }
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        });
    }

    $('.label-info').click( function () {
        $('.label-info').removeClass('tree-selected');
        $(this).addClass('tree-selected');
        console.log($(this).next());
        $('.label-success').removeClass('hidden-copy');
        $(this).next().addClass('hidden-copy');
    });

    $('.fa').click( function() {
        if($(this).hasClass('fa-check-circle-o')){
            $(this).removeClass('fa-check-circle-o');
            $(this).addClass('fa-circle-o');
        } else if($(this).hasClass('fa-circle-o')){
            $(this).removeClass('fa-circle-o');
            $(this).addClass('fa-check-circle-o');
        }
    });

    $('#new-edition').click( function() {
        $('#vcp-editions-list').hide();
        $('#form-vcp-edition').show();
    });

    $('.fa-info-circle').parent().click( function() {
        $('#vcp-editions-list').hide();
        $('#detail-vcp-edition').show();
    });

    $('.amount').keyup( function() {
        real = $(this).val();
        val = real.replace(/,/g, '').replace('.00', '').replace('$','');
        if($.isNumeric(val)){
            n = parseInt(val);
            $(this).val('$'+n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
        } else {
            sub = real.substr(0, real.length - 1);
            $(this).val(sub);
        }
    });

    $('.fa-money').hover(function() {
        $(this).next().show();
    }, function() {
        $(this).next().hide();
    });

    $('.fa-question-circle').click( function() {
        $('#status-widget').modal('show');
    });

});

function showKPIEdit(span) {
    td = $(span).parent().parent().parent();
    name = $(span).data('name');
    td.find('.kpi-edit input').val(name);
    td.find('.kpi-show').hide();
    td.find('.kpi-edit').show();
}

function editKPIShow(span) {
    td = $(span).parent().parent().parent();
    name = $(span).prev().val();
    td.find('.kpi-show span').data('name', name);
    td.find('.kpi-show span b').html(name);
    td.find('.kpi-edit').hide();
    td.find('.kpi-show').show();
}

function addGlobalKPI(span) {
    input = $(span).prev();
    span_parent = $(span).parent();
    if(input.val().length > 3){
        if (span_parent.attr('id') == "add-g-bss") {
            add_g_bss = $('#add-g-bss').parent().parent();
            if(add_g_bss.next().find('.add-kpi').length > 0
                || add_g_bss.next().find('.empty').length > 0) {
                add_g_bss.parent().after(kpi_input({bss: true, sff: false, type: 'g', method: 'addGlobalKPI'}));
            } else {
                add_g_bss.parent().next().children().first().html(kpi_fill({id: 'add-g-bss', placeholder: 'Bussiness', method: 'addGlobalKPI'}));
            }
            group = 'g-bss';
        }
        if (span_parent.attr('id') == "add-g-sff") {
            add_g_sff = $('#add-g-sff').parent().parent();
            if(add_g_sff.prev().find('.add-kpi').length > 0
                || add_g_sff.prev().find('.empty').length > 0) {
                add_g_sff.parent().after(kpi_input({bss: false, sff: true, type: 'g', method: 'addGlobalKPI'}));
            } else {
                add_g_sff.parent().next().children().last().html(kpi_fill({id: 'add-g-sff', placeholder: 'Staff', method: 'addGlobalKPI'}));
            }
            group = 'g-sff';
        }
        span_parent.parent().parent().html(kpi_toggle({name: input.val(), group: group}));
    } else {
        span_parent.removeClass('state-success');
        span_parent.addClass('state-error');
        if(span_parent.parent().find('em').length == 0) {
            span_parent.after('<em for="name" class="invalid">Please enter more than 3 characters</em>');
        }
    }

    resizeWizardHeight();
}

function addIndividualKPI(span) {
    input = $(span).prev();
    span_parent = $(span).parent();
    if(input.val().length > 3){
        if (span_parent.attr('id') == "add-i-bss") {
            add_i_bss = $('#add-i-bss').parent().parent();
            if(add_i_bss.next().find('.add-kpi').length > 0
                || add_i_bss.next().find('.empty').length > 0) {
                add_i_bss.parent().after(kpi_input({bss: true, sff: false, type: 'i', method: 'addIndividualKPI'}));
            } else {
                add_i_bss.parent().next().children().first().html(kpi_fill({id: 'add-i-bss', placeholder: 'Bussiness', method: 'addIndividualKPI'}));
            }
            group = 'i-bss';
        }
        if (span_parent.attr('id') == "add-i-sff") {
            add_i_sff = $('#add-i-sff').parent().parent();
            if(add_i_sff.prev().find('.add-kpi').length > 0
                || add_i_sff.prev().find('.empty').length > 0) {
                add_i_sff.parent().after(kpi_input({bss: false, sff: true, type: 'i', method: 'addIndividualKPI'}));
            } else {
                add_i_sff.parent().next().children().last().html(kpi_fill({id: 'add-i-sff', placeholder: 'Staff', method: 'addIndividualKPI'}));
            }
            group = 'i-sff';
        }
        span_parent.parent().parent().html(kpi_toggle({name: input.val(), group: group}));
    } else {
        span_parent.removeClass('state-success');
        span_parent.addClass('state-error');
        if(span_parent.parent().find('em').length == 0) {
            span_parent.after('<em for="name" class="invalid">Please enter more than 3 characters</em>');
        }
    }
    resizeWizardHeight();
}

function sumDistribution(input, totalDiv) {
    if($.isNumeric( $(input).val() )) {
        total = parseInt($('#'+totalDiv+' input').val());
        if($(input).data('oldvalue') == "") $(input).data('oldvalue', '0');
        total = total - parseInt($(input).data('oldvalue'));
        total = total + parseInt($(input).val());
        $(input).data('oldvalue', $(input).val());
        if(total > 100) {
            val = parseInt($(input).val()) - (total - 100);
            $(input).val(val);
            $(input).data('oldvalue', val);
            $('#'+totalDiv+' input').val('100');
            $('#'+totalDiv+' label').html('100%');
        } else {
            $('#'+totalDiv+' input').val(total);
            $('#'+totalDiv+' label').html(total + '%');
        }
    } else {
        input_parent = $(input).parent();
        input_parent.removeClass('state-success');
        input_parent.addClass('state-error');
        if(input_parent.find('em').length == 0) {
            input_parent.after('<em for="name" class="invalid">Digits only please</em>');
        }
    }
    resizeWizardHeight();
}

function resizeWizardHeight(){
    $('fieldset.current').parent().height($('fieldset.current').innerHeight());
}

function locateGlobalActual(){
    $('.toggle-values').each(function( index ) {
        $(this).position({
            of: $(this).parent().parent().parent(),
            my: "left top",
            at: "left bottom",
            collision: "none none"
        });
        $(this).css({'width':'13em', 'text-align': 'center'});
        $(this).hide();
    });
}