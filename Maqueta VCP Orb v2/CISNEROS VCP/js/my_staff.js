var blue = "#5CD437";
var red = "#DC2656";
var green = "#34D4B9";

$(document).ready(function () {
	// Available Validations
    if ($('#available-validations').length) {

        $("#available-validations").validate({
            // Rules for form validation
            rules: {
                required: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                url: {
                    required: true,
                    url: true
                },
                date3: {
                    required: true,
                    date: true
                },
                min: {
                    required: true,
                    minlength: 5
                },
                max: {
                    required: true,
                    maxlength: 5
                },
                range: {
                    required: true,
                    rangelength: [5, 10]
                },
                digits: {
                    required: true,
                    digits: true
                },
                number: {
                    required: true,
                    number: true
                },
                minVal: {
                    min: 0
                },
                maxVal: {
                    required: true,
                    max: 100
                },
                rangeVal: {
                    required: true,
                    range: [5, 100]
                }
            },

            // Messages for form validation
            messages: {
                required: {
                    required: 'Please enter something'
                },
                email: {
                    required: 'Please enter your email address'
                },
                url: {
                    required: 'Please enter your URL'
                },
                date3: {
                    required: 'Please enter some date in mm/dd/yyyy format'
                },
                min: {
                    required: 'Please enter some text'
                },
                max: {
                    required: 'Please enter some text'
                },
                range: {
                    required: 'Please enter some text'
                },
                digits: {
                    required: 'Please enter some digits'
                },
                number: {
                    required: 'Please enter some number'
                },
                minVal: {
                    required: 'Please enter some value'
                },
                maxVal: {
                    required: 'Please enter some value'
                },
                rangeVal: {
                    required: 'Please enter some value'
                }
            },

            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        });
    }

    //Step Slider

    if ($('.ui-slider').length) {
        $('.slider').each( function(i) {
            slider = $(this);
            slider.find('div').slider({
                min: 0,
                max: 150,
                step: 5,
                slide: function (event, ui) {
                    ui.handle.parentNode.parentNode.parentNode.parentNode.children[2].innerHTML = ui.value;
                }
            });
            slider.find('a').css('left', slider.find('span').text()+'%');
        });
    }

    if($('#staff-1').length) {
        $('#staff-1').click( function() {
            if(!$('#value-1').is(":visible")){
                $('.cold-grey').hide();
                $('#value-1').show();
            }
        });
    }
    if($('#staff-2').length) {
        $('#staff-2').click( function() {
            if(!$('#value-2').is(":visible")){
                $('.cold-grey').hide();
                $('#value-2').show();
            }
        });
    }

    if ($("#staff-1-chart").length) {

        var dates1 = [
            ["Andrew", 130],
            ["David", 95]
        ];

        $.plot("#staff-1-chart", [{
            data: dates1,
            label: "Individual evaluation"
        }], {
            colors: ["#777"],
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 0,
                backgroundColor: "transparent"
            },
            legend: {
                labelBoxBorderColor: false,
            },
            series: {
                bars: {
                    show: true,
                    barWidth: 0.5,
                    fill: 0.8,
                    lineWidth: 0,
                    align: "center"
                }
            },
            xaxis: {
                font: {
                    color: '#555',
                    family: 'Open Sans, sans-serif',
                    size: 11
                },
                mode: "categories",
                tickLength: 0
            },
            yaxis: {
                font: {
                    color: '#333',
                    family: 'Open Sans, sans-serif',
                    size: 11
                }
            }
        });

        $("#staff-1-chart").bind('plothover', function ( event, pos, item ) {
            $('#tooltip').remove();
            if(item){
                var mssg = item.datapoint[1];
                showTooltip(item.pageX, item.pageY, mssg);
            }
        });
    }

    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        content: '<input type="text">'
    });

    $('.comments').click( function() {
        $('#comments').modal('show');
    });

    $('.lock-eval').click( function() {
        if($(this).hasClass('fa-lock')){
            $(this).removeClass('fa-lock');
            $(this).addClass('fa-unlock');
        } else {
            $(this).removeClass('fa-unlock');
            $(this).addClass('fa-lock');
        }
    });

});

function showTooltip(x, y, contents){
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute', display: 'none', top: y - 20, left: x, 
        padding: '2px', opacity: 0.8, 'font-weight': 'bold'
    }).appendTo('body').fadeIn(200);
}