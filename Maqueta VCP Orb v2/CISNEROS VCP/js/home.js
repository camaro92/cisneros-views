var green = "#5CD437";
var red = "#DC2656";
var blue = "#34D4B9";

$(document).ready(function () {
	//PieChart Demo
    var pieData = [{
            value: 10,
            color: green,
            highlight: "#8EE075",
        	label: "10%"
        }, {
            value: 70,
            color: red,
            highlight: "#D65778",
        	label: "70%"
        }, {
            value: 20,
            color: blue,
            highlight: "#63D2BF",
        	label: "20%"
        }
    ];

    $('#kpi1').css('color', green);
    $('#kpi2').css('color', red);
    $('#kpi3').css('color', blue);

    $('.green').css('background-color', green);
    $('.red').css('background-color', red);
    $('.blue').css('background-color', blue);

    var pieChartoptions = {
        animateRotate : true,
        animationEasing: "easeInCubic",
        responsive: true,
        segmentShowStroke: true,
    };

    var kpiChart = null;

	if ($('#kpi-pie').length > 0) {
	    kpiChart = new Chart($("#kpi-pie")[0].getContext("2d")).Pie(pieData, pieChartoptions);
	}

	kpiChart.hover = function(evt){
		var activePoints = kpiChart.getSegmentsAtEvent(evt);
		activePoints.popover();
	}, function() {
		var activePoints = kpiChart.getSegmentsAtEvent(evt);
		activePoints.popover();
	};
});