
$(document).ready( function() {

    var data = [
        {
            period: '2015 Q1',
            GlobalBusinessUnit: 0,
            OperationalUnit: 10,
            LocalBusinessUnit: 0,
            GlobalOC: 0
        }, {
            period: '2015 Q2',
            GlobalBusinessUnit: 20,
            OperationalUnit: 30,
            LocalBusinessUnit: 15,
            GlobalOC: 10
        }, {
            period: '2015 Q3',
            GlobalBusinessUnit: 50,
            OperationalUnit: 80,
            LocalBusinessUnit: 45,
            GlobalOC: 70
        }, {
            period: '2015 Q4',
            GlobalBusinessUnit: 120,
            OperationalUnit: 100,
            LocalBusinessUnit: 90,
            GlobalOC: 98
        }
    ];

	if ($('#vcp-media-chart').length > 0) {
        Morris.Area({
            element: 'vcp-media-chart',
            behaveLikeLine: true,
            data: data,
            lineWidth: '6',
            pointStrokeWidth: '3',
            gridTextFamily: 'Open Sans, sans-serif',
            lineColors: ["#82b964", "#858689", "#993838", "#f87aa0"],
            xkey: 'period',
            ymax: 120,
            gridTextSize: 11,
            ykeys: ['GlobalBusinessUnit', 'OperationalUnit', 'LocalBusinessUnit', 'GlobalOC'],
            labels: ['Global Business Unit', 'Operational Unit', 'Local Business Unit', 'Global OC'],
        }).on('click', function (i, row) {
            console.log(i, row);
        });
    }
    if ($('#vcp-state-chart').length > 0) {
        Morris.Area({
            element: 'vcp-state-chart',
            behaveLikeLine: true,
            data: data,
            lineWidth: '6',
            pointStrokeWidth: '3',
            gridTextFamily: 'Open Sans, sans-serif',
            lineColors: ["#82b964", "#858689", "#993838", "#f87aa0"],
            xkey: 'period',
            ymax: 120,
            gridTextSize: 11,
            ykeys: ['GlobalBusinessUnit', 'OperationalUnit', 'LocalBusinessUnit', 'GlobalOC'],
            labels: ['Global Business Unit', 'Operational Unit', 'Local Business Unit', 'Global OC'],
        }).on('click', function (i, row) {
            console.log(i, row);
        });
    }
    if ($('#vcp-interactive-chart').length > 0) {
        Morris.Area({
            element: 'vcp-interactive-chart',
            behaveLikeLine: true,
            data: data,
            lineWidth: '6',
            pointStrokeWidth: '3',
            gridTextFamily: 'Open Sans, sans-serif',
            lineColors: ["#82b964", "#858689", "#993838", "#f87aa0"],
            xkey: 'period',
            ymax: 120,
            gridTextSize: 11,
            ykeys: ['GlobalBusinessUnit', 'OperationalUnit', 'LocalBusinessUnit', 'GlobalOC'],
            labels: ['Global Business Unit', 'Operational Unit', 'Local Business Unit', 'Global OC'],
        }).on('click', function (i, row) {
            console.log(i, row);
        });
    }
    if ($('#vcp-corp-chart').length > 0) {
        Morris.Area({
            element: 'vcp-corp-chart',
            behaveLikeLine: true,
            data: data,
            lineWidth: '6',
            pointStrokeWidth: '3',
            gridTextFamily: 'Open Sans, sans-serif',
            lineColors: ["#82b964", "#858689", "#993838", "#f87aa0"],
            xkey: 'period',
            ymax: 120,
            gridTextSize: 11,
            ykeys: ['GlobalBusinessUnit', 'OperationalUnit', 'LocalBusinessUnit', 'GlobalOC'],
            labels: ['Global Business Unit', 'Operational Unit', 'Local Business Unit', 'Global OC'],
        }).on('click', function (i, row) {
            console.log(i, row);
        });
    }
});

function showChart(id){
    $(id).children().first().show();
    $(id).children().last().hide();
}

function showTable(id){
    $(id).children().first().hide();
    $(id).children().last().show();
}