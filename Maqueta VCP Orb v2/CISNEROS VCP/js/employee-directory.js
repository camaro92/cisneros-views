$(document).ready( function() {

	$('#add-user').click( function() {
		$('#users-directory').hide();
		$('#user-add-widget').show();
	});

	if ($('#employees-table').length) {
		$('#employees-table').DataTable({
			order: [[0, 'desc']],
			dom: 'Bfrtip',
			buttons: ['pageLength'],
			lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		});

		$('input[type=search]').parent().html($('input[type=search]').parent().find('input'));
		$('input[type=search]').attr('placeholder', 'Search');
	}
});