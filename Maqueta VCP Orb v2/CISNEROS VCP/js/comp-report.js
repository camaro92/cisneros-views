$(document).ready( function() {
	var asInitVals = [];

    if ($('#company-table').length) {
        $('#company-table').DataTable({
            dom: 'Bfrtip',
	        buttons: ['pageLength'],
            lengthMenu: [
	            [ 10, 25, 50, -1 ],
	            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
	        ],
        });

        $('input[type=search]').parent().html($('input[type=search]').parent().find('input'));
        $('input[type=search]').attr('placeholder', 'Search');
    }
});