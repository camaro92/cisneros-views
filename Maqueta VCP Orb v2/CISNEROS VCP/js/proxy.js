$(document).ready(function () {

	$("#select-master").select2({
		ajax: {
			url: "https://api.github.com/search/repositories",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params, // search term
					order: 'desc',
					sort: 'stars'
				};
			},
			processResults: function (data, params) {
				// parse the results into the format expected by Select2
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data, except to indicate that infinite
				// scrolling can be used
				console.log(data);
				params.page = params.page || 1;

				return {
					results: data.items,
					pagination: {
						more: (params.page * 30) < data.total_count
					}
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: formatRepo, // omitted for brevity, see the source of this page
		templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
	});

	//local source
	$("#keys").select2({
		tags: true,
		tokenSeparators: [',', ' '],
		placeholder: "Choose a user",
		tags:["russia", "europe", "obama"]
		// ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
	 //        url: "/foo/bar.json",
	 //        dataType: 'json',
	 //        results: function (data, page) { // parse the results into the format expected by Select2.
	 //            // since we are using custom formatting functions we do not need to alter remote JSON data
	 //            return {results: data};
	 //        }
	 //    },
	});
});

function formatRepo (repo) {
	if (repo.loading) return repo.text;

	var markup = "<div class='select2-result-repository clearfix'>" +
		"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
		"<div class='select2-result-repository__meta'>" +
		"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

	if (repo.description) {
		markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
	}

	markup += "<div class='select2-result-repository__statistics'>" +
		"<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
		"<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
		"<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
		"</div>" +
		"</div></div>";

		return markup;
	}

function formatRepoSelection (repo) {
	return repo.full_name || repo.text;
}